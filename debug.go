package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

func writeFile(content string,customName string){
	name:="log_"+testing+"Test.txt"
	if customName!=""{
		name=customName
	}
	f, err := os.OpenFile(name,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0777)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()
	if _, err := f.WriteString(content); err != nil {
		log.Println(err)
	}
	_=f.Sync()
}


func getTickData(file string)(instrumentData,error){
	b, err := ioutil.ReadFile(file)
	if err != nil {
		fmt.Print(err)
		fmt.Println("No tick data file. Program exiting")
		os.Exit(1)
	}
	lines:=strings.Split(string(b),"\n")
	var ins instrumentData
	ins.Instrument="EUR_USD"
	for i:=0;i<len(lines);i++ {
		var c candle
		data := strings.Split(lines[i], ";")
		if len(lines[i]) > 1 {
			c.Time = fmt.Sprintf("%v-%v-%vT%v:%v:00.000000000Z", data[0][0:4], data[0][4:6], data[0][6:8], data[0][9:11], data[0][11:13])
			c.MidF.O, _ = strconv.ParseFloat(data[1], 64)
			c.MidF.H, _ = strconv.ParseFloat(data[2], 64)
			c.MidF.L, _ = strconv.ParseFloat(data[3], 64)
			c.MidF.C, _ = strconv.ParseFloat(data[4], 64)
			ins.Candles = append(ins.Candles, c)
		}
	}

	return ins,nil
}

func writeCandleToFile(instrumentName string)  {
	ins,_:=getInstrumentCandle(instrumentName,granularity,5000)
	s:=""
	filename:=""
	for i:=0;i<len(ins.Candles);i++ {

		layout := "2006-01-02T15:04:05.000000000Z"
		t1, err := time.Parse(layout, ins.Candles[i].Time)
		if err!=nil{
			fmt.Println(err)
		}
		t1=t1.Add(-5*time.Hour)
		line:=fmt.Sprintf("%d%02d%02d %02d%02d%02d;%v;%v;%v;%v;0\n",	t1.Year(), t1.Month(), t1.Day(),t1.Hour(), t1.Minute(), t1.Second(),ins.Candles[i].Mid.O,ins.Candles[i].Mid.H,ins.Candles[i].Mid.L,ins.Candles[i].Mid.C)
		if i==0 {
			filename+=fmt.Sprintf("%d%02d%02d_%02d%02d%02d-",	t1.Year(), t1.Month(), t1.Day(),t1.Hour(), t1.Minute(), t1.Second())

		}else if i==len(ins.Candles)-1 {
			filename+=fmt.Sprintf("%d%02d%02d_%02d%02d%02d.csv",	t1.Year(), t1.Month(), t1.Day(),t1.Hour(), t1.Minute(), t1.Second())

		}
		s+=line
	}
	name:=filename
	f, err := os.OpenFile(name,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0777)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()
	if _, err := f.WriteString(s); err != nil {
		log.Println(err)
	}
	_=f.Sync()

}