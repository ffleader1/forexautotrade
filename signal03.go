package main

import (
	"math"
)

func signal03Predict(dema decisionMatrix,c []candle) (decisionMatrix,int) {
	if len(dema.nearestHigh) == 0 {
		nearestHigh:= make([]float64, fibonacciGen(fibonacciSearch(dema.noMax) + 1), fibonacciGen(fibonacciSearch(dema.noMax) + 1))
		dema.nearestHigh = append(dema.nearestHigh, nearestHigh...)
	}
	if len(dema.nearestLow) == 0 {
		nearestLow:= make([]float64, fibonacciGen(fibonacciSearch(dema.noMax) + 1), fibonacciGen(fibonacciSearch(dema.noMax) + 1))
		dema.nearestLow = append(dema.nearestLow, nearestLow...)
	}
	var filled bool
	var result int

	for k:=0;k<len(c);k++ {
		filled = true
		if dema.nearestHigh[len(dema.nearestHigh)-1] == 0.0 {
			filled = false
		}
		if !filled {
			for i := 0; i < len(dema.nearestHigh); i++ {
				if dema.nearestHigh[i] == 0.0 {
					dema.nearestHigh[i] = c[k].MidF.H
					break
				}
			}
			for i := 0; i < len(dema.nearestLow); i++ {
				if dema.nearestLow[i] == 0.0 {
					dema.nearestLow[i] = c[k].MidF.L
					break
				}
			}
		} else {
			newHigh := dema.nearestHigh[1:]
			newHigh = append(newHigh, c[k].MidF.H)
			dema.nearestHigh = nil
			dema.nearestHigh = newHigh

			newLow := dema.nearestLow[1:]
			newLow = append(newLow, c[k].MidF.L)
			dema.nearestLow = nil
			dema.nearestLow = newLow
		}

		if !filled {
			return dema, -2
		}
		sumHigh, meanHigh, sumLow, meanLow := 0.0, 0.0, 0.0, 0.0
		for i := 0; i < len(dema.nearestHigh); i++ {
			sumHigh += dema.nearestHigh[i]
			sumLow += dema.nearestLow[i]
		}
		meanHigh = sumHigh / float64(len(dema.nearestHigh))
		meanLow = sumLow / float64(len(dema.nearestLow))
		sumDeviationHigh, meanDeviationHigh, sumDeviationLow, meanDeviationLow := 0.0, 0.0, 0.0, 0.0
		for i := 0; i < len(dema.nearestHigh); i++ {
			sumDeviationHigh += math.Abs(dema.nearestHigh[i]-meanHigh) / meanHigh
			sumDeviationLow += math.Abs(dema.nearestLow[i]-meanLow) / meanLow
		}
		meanDeviationHigh = sumDeviationHigh / float64(len(dema.nearestHigh))
		meanDeviationLow = sumDeviationLow / float64(len(dema.nearestLow))
		//currentDeviationHigh:=dema.nearestHighCache*0.618+meanDeviationHigh*0.382
		//currentDeviationLow:=dema.nearestLowCache*0.618+meanDeviationLow*0.382
		currentDeviationHigh:=dema.nearestHighCache*0.+meanDeviationHigh*1
		currentDeviationLow:=dema.nearestLowCache*0+meanDeviationLow*1
		dema.nearestHighCache=currentDeviationHigh
		dema.nearestLowCache=currentDeviationLow
		if len(c)>1 {
			return dema,-2
		}
		//if currentDeviationHigh*1000.0 < 0.06 && currentDeviationLow*1000.0 > 0.06 {
		//	result=-1
		//}
		//if currentDeviationHigh*1000.0 > 0.06 && currentDeviationLow*1000.0 < 0.06 {
		//	result=1
		//}
		if currentDeviationHigh*1000.0 < 0.05 || currentDeviationLow*1000.0 < 0.05 {
			result=1
		}
		//if strings.Contains(c[0].Time,"2017-01-04T18:43"){
		//	fmt.Println("Óc chó",currentDeviationHigh*1000,currentDeviationLow*1000.0)
		//}
		//testsignal3out += fmt.Sprintln(c[k].Time, currentDeviationHigh*1000.0,currentDeviationLow*1000.0)
	}
	return dema,result

}