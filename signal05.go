package main

import (
	"fmt"
	"math"
)

func signal05Predict(dema decisionMatrix,c []candle) (decisionMatrix,int) {
	if dema.currentTrend != 0 && len(c) < 2 {
		dema.countPatience++
		if !dema.init {
			dema.init = true
			totalRate := dema.totalDeviation
			profitRate, lossRate := 0.0, 0.0
			if dema.currentTrend > 0 {
				profitRate = math.Abs(dema.increasePt)
				lossRate = math.Abs(dema.decreasePt)
			} else {
				lossRate = math.Abs(dema.increasePt)
				profitRate = math.Abs(dema.decreasePt)
			}
			//dema.profitRatio=1
			//dema.lossRatio=1
			profitRate *= 0.618
			lossRate *= 0.382
			//profitRate*=1
			//lossRate*=1
			//profitRate*=0.382
			//lossRate*=0.618
			dema.expectProfit = totalRate / (profitRate + lossRate) * profitRate
			dema.expectLoss = totalRate / (profitRate + lossRate) * lossRate * -1

			fmt.Println("Profit/Loss:", profitRate, lossRate, dema.expectProfit, dema.expectLoss)
			return dema, 0
		}
		//fmt.Println(dema.countPatience)
		nearestDif := c[0].MidF.C - dema.previousClose
		totalDif := c[0].MidF.C - dema.originalRate
		if dema.currentTrend < 0 {
			nearestDif *= -1
			totalDif *= -1
		}

		loss := dema.expectLoss
		profit := dema.expectProfit
		loss *= math.Sin(1 - 1/(math.Pow(float64(dema.countPatience), math.Pi)))
		profit *= math.Sin(1 - 1/(math.Pow(float64(dema.countPatience), math.Pi)))
		if totalDif <= loss || totalDif >= profit {
			if totalDif <= loss {
				lossStop++

			} else if totalDif >= profit {
				profitStop++
			}

			fmt.Println("Expect Profit:", dema.expectProfit, "Expect Loss: ", dema.expectLoss, "Patience Loss: ", dema.countPatienceLoss, "Total Diff: ", totalDif)
			return resetSignal05Data(dema), -2
		}
		//dema.expectLoss *= 0.9382
		//dema.expectProfit *= 0.9618
		dema.expectLoss *= 0.94618
		dema.expectProfit *=0.9382

	}
	return dema, 0
}

func resetSignal05Data(dema decisionMatrix)decisionMatrix{
	dema.init=false
	dema.currentTrend=0
	dema.countPatience=0
	dema.profitRatio=1
	dema.lossRatio=1
	return dema
}