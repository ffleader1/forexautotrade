package main

import (
	"fmt"
	"math"
)

const (
	minProfit = 0.0003
 	maxPatience =4
 	suddenStopRatio=2/3
)
func signal02Predict(dema decisionMatrix,c []candle) (decisionMatrix,int) {
	if dema.currentTrend!=0 && len(c)<2 {
		if !dema.init{
			dema.init=true
			totalRate:=dema.totalDeviation
			profitRate,lossRate:=0.0,0.0
			if dema.currentTrend>0 {
				profitRate= math.Abs(dema.increasePt)
				lossRate= math.Abs(dema.decreasePt)
			}else {
				lossRate= math.Abs(dema.increasePt)
				profitRate= math.Abs(dema.decreasePt)
			}
			//profitRate*=0.618
			//lossRate*=0.382
			//profitRate*=1
			//lossRate*=1
			//profitRate*=0.382
			//lossRate*=0.618
			dema.expectProfit =totalRate/(profitRate+lossRate)  * profitRate
			dema.expectLoss =totalRate/(profitRate+lossRate)  * lossRate*-1
			//fmt.Println("Profit/Loss:",profitRate,lossRate, dema.expectProfit,dema.expectLoss)
			//os.Exit(1)
			return dema,0
		}
		nearestDif :=c[0].MidF.C-dema.previousClose
		totalDif:= c[0].MidF.C - dema.originalRate
		if dema.currentTrend<0 {
			nearestDif *=-1
			totalDif*=-1
		}
		pseudoLost :=0.0
		if nearestDif<0 {
			if dema.accumulatedProfitMultiplier==0 {
				dema.accumulatedProfitMultiplier=1
			}else if dema.accumulatedProfitMultiplier>=2 {
				dema.accumulatedProfitMultiplier =fibonacciGen(fibonacciSearch(dema.accumulatedProfitMultiplier) - 1)
			}
			pseudoLost =float64(dema.accumulatedProfitMultiplier) * nearestDif
		}else {
			if dema.accumulatedProfitMultiplier==0 {
				dema.accumulatedProfitMultiplier=1
			}else if dema.accumulatedProfitMultiplier==1 {
				dema.accumulatedProfitMultiplier=2
			}else {
				dema.accumulatedProfitMultiplier =fibonacciGen(fibonacciSearch(dema.accumulatedProfitMultiplier) + 1)
			}
		}

		if dema.countPatience >=0 {
			dema.countPatience++
			dema=patienceChecker(dema,maxPatience,nearestDif)
		}
		//if strings.Contains(c[0].Time,"2017-04-27T08"){
		//	fmt.Print(c[0].Time,"    ")
		//	fmt.Println("Total diff",totalDif)
		//	//os.Exit(2)
		//}
		if totalDif <=  dema.expectLoss || (pseudoLost <=dema.expectLoss&&totalDif>minProfit) || totalDif>=dema.expectProfit || (nearestDif/totalDif>=suddenStopRatio && totalDif>minProfit) || (dema.countPatience<0 && dema.countPatienceLoss >= maxPatience-1)|| (dema.countPatience>=0 && totalDif< -0.0015)  {
			if totalDif <=  dema.expectLoss {
				lossStop++
			}else if pseudoLost <=dema.expectLoss&&totalDif>minProfit {
				pseudoStop++
			}else if totalDif>=dema.expectProfit {
				profitStop++
			}else if nearestDif/totalDif>=suddenStopRatio && totalDif>minProfit {
				suddenStop++
			}else if dema.countPatience<0 && dema.countPatienceLoss >= maxPatience-1{
				patienceStop++
			}
			fmt.Println("Expect Profit:",dema.expectProfit,"Expect Loss: ", dema.expectLoss,"Patience Loss: ",dema.countPatienceLoss,"Total Diff: ", totalDif)
			return resetSignal02Data(dema),-2
		}
		dema.expectLoss*=0.9618
		dema.expectProfit*=0.9382
		//dema.expectLoss*=0.9382
		//dema.expectProfit*=0.9618
	}
	return dema,0
}

func patienceChecker(dema decisionMatrix,maxCount int, diff float64) decisionMatrix{
	for i:=0;i<maxCount;i++ {
		if dema.countPatience==fibonacciGen(i+3) {
			if diff<0 {
				dema.countPatienceLoss++
			}
			if i==maxCount-1 {
				dema.countPatience=-1
			}
		}
	}
	return dema
}

func resetSignal02Data(dema decisionMatrix)decisionMatrix{
	dema.init=false
	dema.accumulatedProfitMultiplier=0
	dema.currentTrend=0
	dema.countPatience=0
	dema.countPatienceLoss=0
	return dema
}