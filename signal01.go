package main

import (
	"math"
)

//<editor-fold desc="Fibonacci-Golden Ratio Method">
/**
This method takes into account the Fibonacci sequence and the Golden Ratio.
*/
//</editor-fold>

const (
	Golden_ratio = 1.61803398875
	initial_profit_Increasing = 0.001
	initial_profit_Decreasing = 0.0026
	difIncreasingThreshold =0.916
	difDecreasingThreshold=1.832
	Feigenbaum_constant=4.66920
	initial_profit = 0.0013
)


var debugTurnCounter int
//
//func candleRep(c []ohlcF) float64 {
//	O:=c[0].O
//	C:=c[len(c)-1].C
//	H,L:=-10000.0,10000.0
//	for i:=0;i<len(c);i++ {
//		if c[i].H > H {
//			H=c[i].H
//		}
//		if c[i].L<L {
//			L=c[i].L
//		}
//	}
//	M:=(O+C)/2
//	return (C-O)*(H/M+M/L)/2
//}

func sliceSum(s []int) int {
	sum:=0
	for i:=0;i<len(s);i++ {
		sum+=s[i]
	}
	return sum
}
func fibSequence(n int) []int {
	if n==1 {
		return []int{1}
	}else if n==2 {
		return []int{1,1}
	}
	a,b:=1,1
	fibScl :=[]int{1,1}
	for sliceSum(fibScl)+a+b<=n{
		s:=a+b
		fibScl= append(fibScl, s)
		a,b=b,s
	}
	return fibScl
}



func scoreCal(rateVal []float64)float64{
	score:=rateVal[0]
	if len(rateVal)==1{
		return score
	}
	f:= fibSequence(len(rateVal))
	//fmt.Println(f)
	subRateValList :=make([][]float64,0,0)
	t:=0
	scoreList:=make([]float64,len(f),len(f))
	for i:=0;i<len(f);i++ {
		subRateValList = append(subRateValList, rateVal[t:t+f[i]])
		t = t + f[i]
	}
	SUM:=0.0
	for i:=len(scoreList)-1;i>=0;i-- {
		scoreList[i]= scoreCal(subRateValList[i])
		if SUM ==0{
			SUM= scoreList[i]
		}else {
			SUM = SUM*(1-1/Golden_ratio) + scoreList[i]/Golden_ratio
		}
		//SUM+=scoreList[i]

	}
	score= SUM*(1-1/Golden_ratio) + score/Golden_ratio
	return score
}


func statCal(score []float64)(float64,float64,float64){
	s :=0.0
	for i:=0;i<len(score);i++{
		s += score[i]
	}
	average:= s /float64(len(score))
	s=0
	for i:=0;i<len(score);i++ {
		s += math.Pow(average-score[i],2)
	}
	deviation:=math.Sqrt(s/(float64(len(score))-1))
	return average,deviation,deviation/average
}

func signal01Handler(c []candle,noPreviewCandles int,showinfo bool) int {
	return 1
}




func signal01Predict(dema decisionMatrix,c []candle) (decisionMatrix,int) {
	//result := 0
	choice:=0
	cacheTrendLocal := make([]int, 0, 0)
	cacheTrendLocal = append(cacheTrendLocal, dema.cachedTrend...)
	if len(dema.accumulatedTrendDifIncreasing) == 0 {
		accumulatedTrend := make([]int, dema.noMax, dema.noMax)
		dema.accumulatedTrendDifIncreasing = append(dema.accumulatedTrendDifIncreasing, accumulatedTrend...)
	}
	//if len(dema.accumulatedTrendDifDecreasing) == 0 {
	//	accumulatedTrend := make([]int, dema.noMax, dema.noMax)
	//	dema.accumulatedTrendDifDecreasing = append(dema.accumulatedTrendDifDecreasing, accumulatedTrend...)
	//}
	if len(dema.accumulatedDifIncreasing) == 0 {
		accumulatedDif := make([]float64, dema.noMax, dema.noMax)
		dema.accumulatedDifIncreasing = append(dema.accumulatedDifIncreasing, accumulatedDif...)
	}
	//if len(dema.accumulatedDifDecreasing) == 0 {
	//	accumulatedDif := make([]float64, dema.noMax, dema.noMax)
	//	dema.accumulatedDifDecreasing = append(dema.accumulatedDifDecreasing, accumulatedDif...)
	//}


	//for k := 0; k < len(c); k++ {
		//<editor-fold desc="Calculate point between candles">
		//if c[k].MidF.C > c[k].MidF.O {
		//	dema.increaseCon++
		//	dema.decreaseCon = 0
		//	dema.increasePt += math.Pow(c[k].MidF.C/c[k].MidF.O, float64(fibonacciGen(dema.increaseCon)))
		//	if dema.decreasePt*dema.increasePt > 0 {
		//		dema.increasePt -= dema.decreasePt / Golden_ratio
		//	} else if dema.decreasePt*dema.increasePt < 0 {
		//		dema.increasePt += dema.decreasePt / Golden_ratio
		//	}
		//} else if c[k].MidF.C < c[k].MidF.O {
		//	dema.increaseCon = 0
		//	dema.decreaseCon++
		//	dema.decreasePt += math.Pow(c[k].MidF.C/c[k].MidF.O, float64(fibonacciGen(dema.decreaseCon)))
		//	if dema.decreasePt*dema.increasePt > 0 {
		//		dema.decreasePt -= dema.increasePt / Golden_ratio
		//	} else if dema.decreasePt*dema.increasePt < 0 {
		//		dema.decreasePt += dema.increasePt / Golden_ratio
		//	}
		//}
		////</editor-fold>
		//
		//
		//if len(c) < 2 {
		//

			// if choice==0{
				for i := 0; i < dema.noMax; i++ {
					if dema.accumulatedDifIncreasing[i] == 0.0 && dema.previousDif/dema.currentDif >= difIncreasingThreshold  {
						dema.accumulatedDifIncreasing[i] = dema.currentDif
						if dema.increasePt > dema.decreasePt {
							dema.accumulatedTrendDifIncreasing[i] = -1
						} else {
							dema.accumulatedTrendDifIncreasing[i] = 1
						}
						break
					} else {
						if dema.accumulatedDifIncreasing[i] > dema.currentDif || dema.previousDif/dema.currentDif < difIncreasingThreshold  {
							for j := 1; j < dema.noMax; j++ {
								dema.accumulatedDifIncreasing[j] = 0.0
							}
							dema.accumulatedDifIncreasing[0] = dema.currentDif
							if dema.increasePt > dema.decreasePt {
								dema.accumulatedTrendDifIncreasing[0] = -1
							} else {
								dema.accumulatedTrendDifIncreasing[0] = 1
							}
							break
						}
					}
				}
				accumulatedCounter := 0
				for i := 0; i < len(dema.accumulatedDifIncreasing); i++ {
					if dema.accumulatedDifIncreasing[i] > 0.0 {
						accumulatedCounter++
					}
				}
				if accumulatedCounter >= dema.noMax {
					checkDir := true
					for i := 0; i < dema.noMax-1; i++ {
						if dema.accumulatedTrendDifIncreasing[i] != dema.accumulatedTrendDifIncreasing[i+1] {
							checkDir = false
							break
						}
					}
					if checkDir {
						cacheTrendLocal = []int{}
						dema.originalRate = c[0].MidF.C
						debugTurnCounter = 0
						choice =dema.accumulatedTrendDifIncreasing[0]
						dema= resetSignal01Data(dema,[]int{1,-1})
					}else {
						dema= resetSignal01Data(dema,[]int{1})
					}

				}

			//}


			//dema.increasePt *= (100.0 - Golden_ratio) / 100.0
			//dema.decreasePt *= (100.0 - Golden_ratio) / 100.0
			//dema.previousDif = currentDif

		//}
	//}

	dema.cachedTrend = cacheTrendLocal


	return dema, choice

}



func resetSignal01Data(dema decisionMatrix, operation []int)decisionMatrix{
	for i:=0;i<len(operation);i++ {
		if operation[i]==0{
			dema.waitingCount = -1
			dema.waitingCountMax = 0
			dema.previousProfitRate = 0.0
			dema.currentTrend=0
		}else if operation[i]==1{
			for j := 0; j < dema.noMax; j++ {
				dema.accumulatedDifIncreasing[j] = 0
				dema.accumulatedTrendDifIncreasing[j] = 0
			}
		}else if operation[i]==2{
			for j := 0; j < dema.noMax-1; j++ {
				dema.accumulatedDifIncreasing[j] = dema.accumulatedDifIncreasing[j+1]
				dema.accumulatedTrendDifIncreasing[j] = dema.accumulatedTrendDifIncreasing[j+1]
			}
		}
	}
	return dema
}