package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"runtime"
	"strings"
)

func replaceChart(candles []candle,score []float64){
	file, err := os.Open("Candlestick.html")
	if err != nil {
		log.Fatal(err)
	}
	data:=""
	dat := make([]ohlcF, 0, 0)
	for i := 0; i < len(candles); i++ {
		dat = append(dat, candles[i].MidF)
	}
	for i:=0;i<len(score);i++ {
		s := fmt.Sprintf("['%v', %v, %v, %v, %v]", score[i], dat[i].L, dat[i].O,dat[i].C, dat[i].H)
		if i != len(score)-1 {
			s += ","
		}
		data+=s
	}
	scanner := bufio.NewScanner(file)
	filecontent:=make([]string,0,0)
	for scanner.Scan()  {
		line:=scanner.Text()
		if strings.Contains(line,`let data = google.visualization.arrayToDataTable`){
			line=`		            let data = google.visualization.arrayToDataTable([`+data+` ], true);`
			fmt.Println("Replaced")
		}
		filecontent=append(filecontent, line)
	}
	_=file.Close()


	f, err := os.OpenFile("Candlestick.html", os.O_CREATE|os.O_TRUNC, 0777)
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()

	for _,line :=range filecontent{
		_, _ = fmt.Fprintln(f, line)
	}
	_ = f.Sync()

}

//func showChartSignal01(c []candle){
//	noPreviewCandles := 200
//	midData := make([][]ohlcF, noPreviewCandles, noPreviewCandles)
//	for i:=0;i<noPreviewCandles;i++ {
//		for j:=0+i;j<len(c);j++ {
//			midData[i]= append(midData[i], c[j].MidF)
//		}
//	}
//	graphcandles:=make([]candle,noPreviewCandles,noPreviewCandles)
//	graphscore:=make([]float64,noPreviewCandles,noPreviewCandles)
//	for i:=0;i<noPreviewCandles;i++{
//		score := scoreCal(midData[i])
//		graphcandles[noPreviewCandles-i-1]=c[i]
//		graphscore[noPreviewCandles-i-1]= math.Floor(score*10000) / 10000
//	}
//	replaceChart(graphcandles,graphscore)
//	openBrowser("Candlestick.html")
//
//}

func openBrowser(url string) {
	var err error
	webId:=[]string{"www","http,","localhost","127.0.0.1",".com",".net",".org"}
	isWeb:=false
	for i:=0;i<len(webId);i++ {
		if strings.Contains(url, webId[i]){
			isWeb=true
		}
	}
	if !isWeb {
		path := ""
		path, err = os.Getwd()
		if err != nil {
			log.Println(err)
			return
		}
		url = path + "/" + url
	}

	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	if err != nil {
		log.Fatal(err)
	}
}