package main

import (
	"fmt"
	"math"
	"os"
	"time"
)

type instrumentData struct {
	Instrument string `json:"instrument"`
	Granularity string `json:"granularity"`
	Candles []candle `json:"candles"`
}

type candle struct {
	Complete bool `json:"complete"`
	Volume string `json:"type"`
	Time string `json:"time"`
	Mid ohlcStr `json:"mid"`
	MidF ohlcF
}

type ohlcStr struct{
	O string `json:"o"`
	H string `json:"h"`
	L string `json:"l"`
	C string `json:"c"`
}

type ohlcF struct {
	O float64
	H float64
	L float64
	C float64
}

func flipCandles(c[]candle)[]candle{
	nC:=make([]candle,len(c),len(c))
	for i:=0;i<len(c);i++{
		nC[i]=c[len(c)-1-i]
	}
	return nC
}

func parseTimeOanda(timeStr string, deviation time.Duration) (time.Time,string) {
	layout := "2006-01-02T15:04:05.000000000Z"
	t, err := time.Parse(layout,timeStr)
	if err != nil {
		fmt.Println(err)
	}
	t=t.Add(deviation*time.Hour)
	return t,fmt.Sprint(t.Format(layout))
}
//<editor-fold desc="Old auto-create order">
func autoCreateOrder(insName string)  {
	start := time.Now().Add(time.Minute*-1)
	wait:=granularityTime(granularity)
	countCandle:=0
	activeTrade:=false
	for {
		elapsed := time.Since(start)
		if elapsed > wait {
			if tradingTime("") {
				ins, e := getInstrumentCandle(insName, granularity,5000)
				if e != nil {
					wait = time.Second * 30
				} else {
					currentCandles := make([]candle, 0, 0)
					if ins.Candles[len(ins.Candles)-1].Complete {
						currentCandles = append(currentCandles, ins.Candles...)
					} else {
						currentCandles = append(currentCandles, ins.Candles[:len(ins.Candles)-1]...)
					}

					r := signal01Handler(flipCandles(currentCandles), countCandle,true)
					id, tradeIns, unit,unrealized,margin := getTrade()
					for i:=0;i<len(id);i++{
						if tradeIns[i] ==insName{
							if unrealized[i]/margin[i]< -0.3211 {
								mailLossNotification(unrealized[i]/margin[i])
								os.Exit(10)
							}
							break
						}
					}
					countCandle++
					if r == 1 {
						if len(id) < 1 || (len(id) >= 1 && unit[0] < 0) {
							if len(id) != 0 && unit[0] < 0 {
								cancelTrade("")
								activeTrade=false
							}
							TP, SL := getTPSL("EUR_USD")
							nav,_:=getAccountInfo()
							unit:=int(nav*23/5) //leverage 10:1
							createOrder("EUR_USD", unit, TP, SL)
							countCandle = 0
							activeTrade=true
							//recentSign=1
						}

					} else if r == -1 {
						if len(id) < 1 || (len(id) >= 1 && unit[0] > 0) {
							if len(id) != 0 && unit[0] > 0 {
								cancelTrade("")
								activeTrade=false
							}
							TP, SL := 1.0,1.6
							nav,_:=getAccountInfo()
							unit:=int(nav*23/5) //leverage 10:1
							createOrder("EUR_USD", -unit, TP, SL)
							countCandle = 0
							activeTrade=true
							//recentSign=-1
						}

					} else if r == -2 {
						cancelTrade("")
						countCandle = 0
						activeTrade=false
					}

				}
			}else {
				if activeTrade {
					cancelTrade("")
					countCandle = 0
					activeTrade=false
				}
			}
			start = time.Now()
		}
	}
}
//</editor-fold>

func newAutoCreateOrder(instrumentName string){
	var dema decisionMatrix
	dema.noMax=5
	cmd:=0
	candles:=make([]candle,0,0)
	candles = getTimeCalibratedCandles(candles)
	dema , _= signal01Predict(dema,candles)
	orderType:=0
	messArray:=make([]string,0,0)
	mess:=""
	if tradingTime("") {
		for {
			candles = getTimeCalibratedCandles(candles)
			var c candle
			c=candles[0]
			_, c.Time= parseTimeOanda( c.Time,-5)
			dema, cmd= signal01Predict(dema,[]candle{c})
			//fmt.Println("At", candles[0].Time," Increase  Point:",dema.increasePt," Decrease  Point:",dema.decreasePt," Trend:", dema.accumulatedDif)
			if math.Abs(float64(cmd)) == 1 && orderType != cmd {
				TP, SL := getTPSL("EUR_USD")
				nav,_:=getAccountInfo()
				unit:=int(nav*23)//leverage 50:1
				createOrder("EUR_USD", unit*cmd, TP, SL)
				orderType = cmd
			} else if orderType != 0 && cmd == -2 {
				cancelTrade("")
				orderType = 0
			}
			fmt.Print(mess)
			messArray=append(messArray, mess)
			if len(messArray)>=60{
				loghandler(c.Time,messArray)
				messArray=nil
			}
		}
	}

}

func tradingTime(inputTime string)bool{
	var weekday time.Weekday
	var timeString string
	if inputTime=="" {
		weekday = time.Now().Weekday()
		timeString = fmt.Sprintf("%02v:%02v:%02v", time.Now().Hour(), time.Now().Minute(), time.Now().Second())
	}else {
		layout := "2006-01-02T15:04:05.000000000Z"
		t1, err := time.Parse(layout, inputTime)
		if err!=nil{
			fmt.Println(err)
		}
		weekday = t1.Weekday()
		timeString = fmt.Sprintf("%02v:%02v:%02v", t1.Hour(), t1.Minute(), t1.Second())
	}
	layout := "15:04:05"
	if weekday.String() == "Saturday" {
		return false
	}else if weekday.String() == "Friday"{
		t0, _ := time.Parse(layout, "13:30:00")
		t1, err := time.Parse(layout, timeString)
		if err != nil {
			fmt.Println(err)
		}
		if t1.After(t0) {
			return false
		}else {
			return true
		}
	}else if weekday.String() == "Sunday"{
		t0, _ := time.Parse(layout, "16:30:00")
		t1, err := time.Parse(layout, timeString)
		if err != nil {
			fmt.Println(err)
		}
		if t1.After(t0) {
			return true
		}else {
			return false
		}
	}
	return true
}

func main() {
	//difDecreasingThreshold=2.0
	//difDecreasingInitialProfit=0.00028

	//newAutoCreateOrder("EUR_USD")

	//testSignal01(originalFund, "20200714_023700-20200717_155900.csv")
	//testSignal01(originalFund, "DAT_ASCII_EURUSD_M1_202004.csv")
	//
	testSignal01(originalFund, "DAT_ASCII_EURUSD_M1_2017.csv")

	//fmt.Println(tl)

	//writeCandleToFile("EUR_USD")

	//count:=0
	//totalProfit:=0.0
	//totalTopLost :=make([]float64,0,0)
	//totalTopProfit :=make([]float64,0,0)
	//for i:=0;i<=10;i++ {
	//	fund:=10000.0
	//	filename:=fmt.Sprintf("DAT_ASCII_EURUSD_M1_20%02d.csv",i+9)
	//	profit, topLost,topProfit:=testSignal01(10000, filename)
	//	if profit>0 {
	//		count++
	//		fmt.Printf("Profit: %.02f\n",profit*100/fund)
	//	}else {
	//		fmt.Printf("Lost: %.02f\n",profit*100/fund)
	//	}
	//	fmt.Println(topLost[:10])
	//	fmt.Println(topProfit[:10])
	//	sumTopLost:=0.0
	//	for i:=0;i<len(topLost);i++ {
	//		sumTopLost+= topLost[i]
	//	}
	//	fmt.Println("Average lost: ",sumTopLost/float64(len(topLost)))
	//	totalTopLost = append(totalTopLost, topLost...)
	//	sumTopProfit:=0.0
	//	for i:=0;i<len(topProfit);i++ {
	//		sumTopProfit+= topProfit[i]
	//	}
	//	fmt.Println("Average profit: ",sumTopProfit/float64(len(topProfit)))
	//	totalTopProfit = append(totalTopProfit, topProfit...)
	//	totalProfit+=profit
	//}
	//sumTotalTopLost:=0.0
	//for i:=0;i<len(totalTopLost);i++ {
	//	sumTotalTopLost+= totalTopLost[i]
	//}
	//sumTotalTopProfit:=0.0
	//for i:=0;i<len(totalTopProfit);i++ {
	//	sumTotalTopProfit+= totalTopProfit[i]
	//}
	//fmt.Println("Total average lost: ",sumTotalTopLost/float64(len(totalTopLost)))
	//fmt.Println("Total average profit: ",sumTotalTopProfit/float64(len(totalTopProfit)))
	//fmt.Printf("Profitable year: %d/11\nIn total: %.02f\n",count,totalProfit)




	//countRow:=0
	//countColumn:=0
	//profitSlice:=make([]float64,0,0)
	//startThreshold:=1.0
	//endThreshold:=4.0//3.0
	//startInitialProfit:=0.001
	//endInitialProfit:=0.004
	//for difDecreasingInitialProfit=startInitialProfit;difDecreasingInitialProfit<=endInitialProfit+0.000001;difDecreasingInitialProfit+=0.0001 {
	//	countColumn++
	//
	//	for difDecreasingThreshold=startThreshold;difDecreasingThreshold<=endThreshold+0.000001;difDecreasingThreshold+=0.1 {
	//		if  difDecreasingInitialProfit==startInitialProfit {
	//			countRow++
	//		}
	//		totalProfit := 0.0
	//		noLost:=true
	//		for i := 0; i <= 10; i++ {
	//			fund := 10000.0
	//			filename := fmt.Sprintf("DAT_ASCII_EURUSD_M1_20%02d.csv", i+9)
	//			profit, _, _ := testSignal01(fund, filename)
	//			totalProfit += profit
	//			if profit<0 {
	//				noLost=false
	//				break
	//			}
	//		}
	//		if noLost {
	//			profitSlice = append(profitSlice, totalProfit)
	//		}else {
	//			profitSlice = append(profitSlice, 0)
	//		}
	//
	//	}
	//
	//}
	//s:=""
	//for i:=-1;i<countRow;i++{
	//	if i==-1 {
	//		s+="\t"
	//	}else {
	//		s+=fmt.Sprintf(",%.2f",startThreshold+float64(i)*0.1)
	//	}
	//}
	//s+="\n"
	//currentCell:=0
	//for i:=0;i<countColumn;i++ {
	//	for j := -1; j < countRow; j++ {
	//		if j == -1 {
	//			s +=fmt.Sprintf("%.4f",startInitialProfit+float64(i)*0.0001)
	//		}else {
	//			s+=fmt.Sprintf(",%.2f",profitSlice[currentCell])
	//			currentCell++
	//		}
	//	}
	//	s+="\n"
	//}
	//writeLog("Log2",s)
}