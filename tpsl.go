package main

import (
	"fmt"
	"math"
	"time"
)

func getTPSL(insName string)(float64,float64){
	secMAX,minMAX,hourMAX:=-99.9,-99.9,-99.9
	secMIN,minMIN,hourMIN:=99.9,99.9,99.9
	var sec, min, hour bool
	loopBreaker :=0
	start := time.Now().Add(time.Millisecond*-500)
	for {
		if !sec && time.Since(start)>= time.Millisecond*500 {
			r, e := getInstrumentCandle(insName, "S5",5000)
			if e!=nil{
				fmt.Println(e)
				loopBreaker++
			}else {
				for i:=0;i<len(r.Candles);i++ {
					if secMAX<r.Candles[i].MidF.H {
						secMAX=r.Candles[i].MidF.H
					}
					if secMIN>r.Candles[i].MidF.L {
						secMIN=r.Candles[i].MidF.L
					}
				}

				sec=true
			}
			start = time.Now()
		}
		if !min && time.Since(start)>= time.Millisecond*500 {
			r, e := getInstrumentCandle(insName, "M30",5000)
			if e!=nil{
				fmt.Println(e)
				loopBreaker++
			}else {
				for i:=0;i<len(r.Candles);i++ {
					if minMAX<r.Candles[i].MidF.H {
						minMAX=r.Candles[i].MidF.H
					}
					if minMIN>r.Candles[i].MidF.L {
						minMIN=r.Candles[i].MidF.L
					}
				}
				min=true
			}
			start = time.Now()
		}
		if !hour && time.Since(start)>= time.Millisecond*500  {
			r, e := getInstrumentCandle(insName, "H6",5000)
			if e!=nil{
				fmt.Println(e)
				loopBreaker++
			}else {
				for i:=0;i<len(r.Candles);i++ {
					if hourMAX<r.Candles[i].MidF.H {
						hourMAX=r.Candles[i].MidF.H
					}
					if hourMIN>r.Candles[i].MidF.L {
						hourMIN=r.Candles[i].MidF.L
					}
				}
				hour=true
			}
			start = time.Now()
		}
		if sec && min && hour {
			break
		}
		if loopBreaker >5 {
			return -1,-1
		}

	}


	TP:=(minMAX/Golden_ratio + (1-1/Golden_ratio)*hourMAX)/Golden_ratio  + (1-1/Golden_ratio)*secMAX
	SL:=(minMIN/Golden_ratio + (1-1/Golden_ratio)*hourMIN)/Golden_ratio  + (1-1/Golden_ratio)*secMIN
	TP = math.Floor(TP*100) / 100
	SL = math.Floor(SL*100) / 100
	return TP,SL
}
