package main

const(
	RSIPeriod = 14.0
)

func RSICal(dema decisionMatrix,c candle)(decisionMatrix,float64){
	sumGain, sumLost:=0.0,0.0
	dif:= c.MidF.C-c.MidF.O
	if dif>0{
		sumGain+=dif
	}else {
		sumLost-=dif
	}
	dema.previousGain=(dema.previousGain*(RSIPeriod-1)+sumGain)/RSIPeriod
	dema.previousLost=(dema.previousLost*(RSIPeriod-1)+sumLost)/RSIPeriod
	if dema.previousGain==0.0{
		return dema,0
	}else if dema.previousLost==0.0{
		return dema,100
	}
	return dema,100-(100/(1+dema.previousGain/dema.previousLost))
}

func predictSignal04Candle(dema decisionMatrix,candles []candle)(decisionMatrix, int){
	RSI:=-1.0
	for i:=0;i<len(candles);i++ {
		if !dema.startedRSI {
			if len(dema.savedCandles) <  RSIPeriod{
				dema.savedCandles= append(dema.savedCandles, candles[i])
			}else if len(dema.savedCandles)==RSIPeriod {
				sumGain, sumLost := 0.0, 0.0
				for j := 0; j < len(dema.savedCandles); j++ {
					dif := dema.savedCandles[j].MidF.C -  dema.savedCandles[j].MidF.O
					if dif > 0 {
						sumGain += dif
					} else {
						sumLost -= dif
					}
				}
				dema.previousGain=sumGain/14
				dema.previousLost=sumLost/14
				dema.startedRSI=true
			}
		}else {
			dema,RSI=RSICal(dema,candles[i])
		}
	}

	if RSI <0 {
		return dema,-2
	}else if RSI<=30 {
		return dema,-1
	}else if RSI >=70 {
		return dema, 1
	}
	return dema,0
}
