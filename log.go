package main

import (
	"fmt"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"log"
	"math"
	"os"
	"strings"
)

const SENDGRID_API_KEY = `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`
func mailLossNotification(lossRate float64) {
	from := mail.NewEmail("ffleader1", "ffleader1@gmail.com")
	subject := "FOREX STRONG LOST NOTIFICATION"
	to := mail.NewEmail("ffleader1", "ffleader1@gmail.com")
	s:= fmt.Sprintf("Recently, a trade has lost over %.2v%% margin.\n",math.Abs(lossRate)*100)
	s1:="BOT STOPPED BUT TRADE IS NOT CLOSED. TAKE ACTION ASAP"
	s2:="<strong>BOT STOPPED BUT TRADE IS NOT CLOSED. TAKE ACTION ASAP.</strong>"
	plainTextContent := s+s1
	htmlContent := s+s2
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)
	client := sendgrid.NewSendClient(SENDGRID_API_KEY)
	response, err := client.Send(message)
	if err != nil {
		log.Println(err)
	} else {
		fmt.Println(response.StatusCode)
		fmt.Println(response.Body)
		fmt.Println(response.Headers)
	}
}

func loghandler(time string, content []string)  {
	time=strings.Split(time,".0")[0]
	time=strings.Replace(time,":",".",-1)
	title:= "FOREX UPDATE NOTIFICATION AT " + time
	s:=""
	for i:=0;i<len(content);i++{
		s+=content[i]
	}
	//fmt.Println("Log created")
	mailHourlyUpdateNotification(title,s)
	writeLog(title,s)
}

func mailHourlyUpdateNotification(title, content string) {
	from := mail.NewEmail("ffleader3", "ffleader3@gmail.com")
	subject := title
	to := mail.NewEmail("ffleader0", "ffleader0@gmail.com")
	plainTextContent := content
	htmlContent := strings.Replace(content,"\n",`<br>`,-1)
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)
	client := sendgrid.NewSendClient(SENDGRID_API_KEY)
	response, err := client.Send(message)
	if err != nil {
		log.Println(err)
	} else {
		if response.StatusCode != 202 {
			fmt.Println(response.StatusCode)
			fmt.Println(response.Body)
			fmt.Println(response.Headers)
		}
	}
}

func writeLog(filename, content string){
	if _, err := os.Stat("Log"); os.IsNotExist(err) {
		os.Mkdir("Log", 0777)
	}
	name:=`Log/`+filename+`.txt`
	f, err := os.OpenFile(name,
		os.O_TRUNC|os.O_CREATE|os.O_WRONLY, 0777)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()
	if _, err := f.WriteString(content); err != nil {
		log.Println(err)
	}
	_=f.Sync()
}