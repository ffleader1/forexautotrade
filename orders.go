package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty"
	"github.com/tidwall/gjson"
	"strconv"
	"strings"
	"time"
)

const (
	TOKEN       = "76dc6760a153e7e3dc4add4e1ba227c0-ece8d2230e2a6352085e78260a592df3" //trade practice: ffleader1
	AccountId   = "101-001-14789826-001" //trade practice: ffleader1
	//TOKEN =`ad1403a236905925a0323e61edca0b31-171238502135db4d7f39c4ac0d1c6d82`
	//AccountId=`101-011-15182554-001` //trade practice: nnt296
	//TOKEN = `ced387fd6fe3cf80c37afc3209f5b9dd-fe751d4702321bb75d94348b7773afd1` //Real account ffleader0
	//AccountId=`001-011-4317300-001` // Real account ffleader0
	//APIUrl = `https://api-fxtrade.oanda.com/v3`
	APIUrl = `https://api-fxpractice.oanda.com/v3`
	granularity = `m1`
)

func granularityTime(gran string) time.Duration {
	c:=gran[0:1]

	var d time.Duration
	if c=="S"||c=="s" {
		d=time.Second
	}else if c=="M"||c=="m"{
		d=time.Minute
	}else if c=="H"||c=="h"{
		d=time.Hour
	}else if c=="D"||c=="d"{
		return time.Hour*24
	}
	n:=gran[1:]
	i,e:=strconv.Atoi(n)
	if e!=nil{
		fmt.Println(e)
		return time.Minute*15
	}
	return d*time.Duration(i)

}

func getInstrumentCandle(ins,gran string,noCandles int) (instrumentData,error) { //crucial
	url:=APIUrl+"/instruments/"+ins+"/candles"
	client := resty.New()
	resp, err := client.R().
		SetAuthToken(TOKEN).
		SetQueryParam("granularity",strings.ToUpper(gran)).
		SetQueryParam("count",fmt.Sprint(noCandles)).
		Get(url)

	C:=&instrumentData{}

	if err!=nil{
		return *C,err
	}
	body:=resp.String()
	requestError:=gjson.Get(body,"errorMessage").Str
	if requestError!=""{
		return *C,errors.New(requestError)
	}
	e:=json.Unmarshal([]byte(body), &C)
	if e!=nil {
		return *C,e

	}
	for i:=0;i<len(C.Candles);i++ {
		C.Candles[i].MidF.O,_=strconv.ParseFloat(C.Candles[i].Mid.O,64)
		C.Candles[i].MidF.H,_=strconv.ParseFloat(C.Candles[i].Mid.H,64)
		C.Candles[i].MidF.C,_=strconv.ParseFloat(C.Candles[i].Mid.C,64)
		C.Candles[i].MidF.L,_=strconv.ParseFloat(C.Candles[i].Mid.L,64)
	}
	return *C,nil
}

func getCurrentPrice(ins string)(float64,error){
	url:=APIUrl+"/instruments/"+ins+"/candles"
	client := resty.New()
	resp, err := client.R().
		SetAuthToken(TOKEN).
		SetQueryParam("granularity","S5").
		SetQueryParam("count","500").
		Get(url)
	C:=&instrumentData{}

	if err!=nil{
		return -1,err
	}
	body:=resp.String()
	requestError:=gjson.Get(body,"errorMessage").Str
	if requestError!=""{
		return -1,errors.New(requestError)
	}
	e:=json.Unmarshal([]byte(body), &C)
	if e!=nil {
		return -1,e
	}
	result,_:=strconv.ParseFloat(C.Candles[len(C.Candles)-1].Mid.C,64)
	return result,nil
}

type order struct {
	Type string `json:"type"`
	Instrument string `json:"instrument"`
	Unit int `json:"units"`
	TimeInForce string `json:"timeInForce"`
	TakeProfitOnFill  tpsl `json:"takeProfitOnFill"`
	StopLossOnFill  tpsl `json:"stopLossOnFill"`
}
type tpsl struct {
	Price string  `json:"price"`
}
func RoundFloat(x float64, prec int) float64 {
	frep := strconv.FormatFloat(x, 'g', prec, 64)
	f, _ := strconv.ParseFloat(frep, 64)
	return f
}
func createOrder(instrument string, Unit int, TP,SL float64) string {
	url:=APIUrl+"/accounts/"+ AccountId +"/orders"
	client := resty.New()
	var o order
	o.Type="MARKET"
	o.Instrument=instrument
	o.Unit=Unit
	o.TimeInForce="FOK"
	fTP,fSL:=0.0,0.0
	if TP>SL {
		fTP=TP
		fSL=SL
	}else {
		fTP=SL
		fSL=TP
	}

	if Unit>=0 {
		o.TakeProfitOnFill.Price = fmt.Sprintf("%0.2f",fTP)
		o.StopLossOnFill.Price = fmt.Sprintf("%0.2f",fSL)
	}else{
		o.TakeProfitOnFill.Price = fmt.Sprintf("%0.2f",fSL)
		o.StopLossOnFill.Price = fmt.Sprintf("%0.2f",fTP)
	}

	fmt.Println(o)
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(map[string]interface{}{"order": o}).
		SetAuthToken(TOKEN).
		Post(url)
	if err!=nil{
		fmt.Println(err)
	}else {
		fmt.Println("Create Order Sent")
		fmt.Println(string(resp.Body()))
		return string(resp.Body())
	}
	return ""
}

func getTrade()([]string,[]string,[]float64,[]float64,[]float64){
	url:=APIUrl+"/accounts/"+ AccountId +"/trades"
	client := resty.New()
	resp, err := client.R().
		SetAuthToken(TOKEN).
		Get(url)
	if err!=nil {
		fmt.Println(err)
	}else {
		trades:=gjson.Get(string(resp.Body()),"trades").Array()
		id,ins:=make([]string,0,0),make([]string,0,0)
		unit:=make([]float64,0,0)
		unrealized:=make([]float64,0,0)
		margin:=make([]float64,0,0)
		for i:=0;i<len(trades);i++ {
			id = append(id, gjson.Get(trades[i].String(),"id").Str)
			ins=append(ins,gjson.Get(trades[i].String(),"instrument").Str)
			unitValue:=gjson.Get(trades[i].String(),"currentUnits").Str
			unrealizedPL:=gjson.Get(trades[i].String(),"unrealizedPL").Str
			marginUsed:=gjson.Get(trades[i].String(),"marginUsed").Str
			f,e:=strconv.ParseFloat(unitValue,64)
			if e!=nil{
				fmt.Println(e)
				unit= append(unit, 0)
			}else {
				unit = append(unit,f)
			}
			f2,e:=strconv.ParseFloat(unrealizedPL,64)
			if e!=nil{
				fmt.Println(e)
				unrealized= append(unrealized, 0)
			}else {
				unrealized = append(unrealized,f2)
			}
			f3,e:=strconv.ParseFloat(marginUsed,64)
			if e!=nil{
				fmt.Println(e)
				margin= append(margin, 0)
			}else {
				margin = append(margin,f3)
			}
		}
		return id,ins,unit,unrealized,margin
	}
	return []string{},[]string{},[]float64{},[]float64{},[]float64{}
}

func cancelTrade(tradeID string)  {
	tradeList:=make([]string,0,0)
	if tradeID==""{
		idList,_,_,_,_:=getTrade()
		tradeList= append(tradeList, idList...)
	}else {
		tradeList= append(tradeList, tradeID)
	}
	for i:=0;i<len(tradeList);i++ {
		url := APIUrl+"/accounts/" + AccountId + "/trades/" +tradeList[i]+"/close"
		client := resty.New()
		resp, err := client.R().
			SetHeader("Content-Type", "application/json").
			SetQueryParam("units","ALL").
			SetAuthToken(TOKEN).
			Put(url)
		if err!=nil {
			fmt.Println(err)
		}else {
			body:=resp.String()
			requestError:=gjson.Get(body,"errorMessage").Str
			if requestError!=""{
				fmt.Println(requestError)
			}
		}
	}
}

func getAccountInfo()(float64,float64){
	url:=APIUrl+"/accounts/"+ AccountId +"/summary"
	client := resty.New()
	resp, err := client.R().
		SetAuthToken(TOKEN).
		Get(url)
	if err!=nil {
		fmt.Println(err)
	}else {
		account:=gjson.Get(string(resp.Body()),"account").String()
		balance:=gjson.Get(account,"balance").Str
		nav:=gjson.Get(account,"NAV").Str
		navF,_:=strconv.ParseFloat(nav,64)
		balanceF,_:=strconv.ParseFloat(balance,64)
		return navF,balanceF
	}
	return 0,0
}

func getTimeCalibratedCandles(c []candle)[]candle{
	var candles []candle
	if len(c)<1{
		var cacheTime string
		for {
			ins,err:=getInstrumentCandle("EUR_USD",granularity,5000)
			if err==nil {
				if ins.Candles[len(ins.Candles)-1].Complete {
					candles= append(candles, ins.Candles...)
					break
				}else {
					if cacheTime=="" {
						cacheTime= ins.Candles[len(ins.Candles)-1].Time
					}else if cacheTime!=ins.Candles[len(ins.Candles)-1].Time  {
						candles= append(candles, ins.Candles...)
						break
					}
					time.Sleep(1*time.Second)

				}
			}else {
				time.Sleep(500*time.Millisecond)
			}
		}
	}else {
		var cacheTime string
		loopCount:=0
		for {
			ins,err:=getInstrumentCandle("EUR_USD",granularity,500)
			if err==nil {
				if ins.Candles[len(ins.Candles)-1].Complete  && ins.Candles[len(ins.Candles)-1].Time!=c[len(c)-1].Time  {
					candles= append(candles, ins.Candles[len(ins.Candles)-1])
					break
				}else {
					if cacheTime=="" {
						cacheTime= ins.Candles[len(ins.Candles)-1].Time
					}else if cacheTime!=ins.Candles[len(ins.Candles)-1].Time  {
						candles= append(candles, ins.Candles[len(ins.Candles)-1])
						break
					}
					if loopCount==0 {
						time.Sleep(50 * time.Second)
						loopCount++
					}else {
						//loopCount++
						time.Sleep(500*time.Millisecond)
					}
				}
			}else {
				time.Sleep(500*time.Millisecond)
			}
		}
	}
	return candles
}