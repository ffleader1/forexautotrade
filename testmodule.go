package main

import (
	"fmt"
	"math"
	"math/rand"
	"time"
)

const testing ="Signal1"
//const tradeCost  = 0.12 //ratio 1:50
const leverage= 50.0
const tradeFee= 0.0006
const enablePermanentFund = false
const originalFund =100.0
var (
	lossStop     int
	pseudoStop   int
	profitStop   int
	suddenStop   int
	patienceStop int
	suddenExit bool
)

func testOrder(orderType int,fund,start,end float64) float64 {
	if orderType==0 {
		return fund
	}
	return fund-fund*tradeFee+(fund-fund*tradeFee)*(end-start)*float64(orderType)*leverage
}


func signalRandomHandler()int{
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	r:=r1.Intn(10)
	if r%10<2 {
		return -1
	}else if r%10>7 {
		return 1
	}else if r==5{//||r==4||r==6 {
		return -2
	}
	return 0
}


func testTradeResult(old,new float64,orderType int,countResult []int) (float64,[]int,bool)  {
	var profitable bool
	if old>new {
		fmt.Printf("Incorrect order. Before: %.02f$ - After: %.02f$. Loss: %.04f%%\n",old,new,math.Abs(old-new)*100/old)
		if orderType==1 {
			countResult[2]++
		}else {
			countResult[3]++
		}
		if new <0 {
			new=0
		}
		if math.Abs(old-new)*100/old >20 {
			suddenExit=true
		}
	}else if old <  new {
		fmt.Printf("Correct order. Before: %.02f$ - After: %.02f$. Profit: %.04f%%\n",old,new,math.Abs(old-new)*100/old)
		if orderType==1 {
			countResult[0]++
		}else {
			countResult[1]++
		}
		profitable=true
	}
	return new,countResult,profitable
}

//var testsignal3out string
func testSignal01(fund float64,file string) (float64,[]float64,[]float64)  {
	var instrument instrumentData
	var e error
	if file=="" {
		instrument, e = getInstrumentCandle("EUR_USD", granularity,5000)
	}else {
		instrument, e = getTickData(file)
	}
	if e!=nil{
		fmt.Println(e)
	}
	var permaFund float64
	original:=fund
	offset:=2000
	var dema decisionMatrix
	dema.noMax=5
	//cachedTrend:=make([]string,0,0)
	cmd:=0
	//accumulatedTrend:=make([]int,dema.noMax,dema.noMax)
	//dema.accumulatedTrend=append(dema.accumulatedTrend,accumulatedTrend...)
	//accumulatedDif :=make([]float64,dema.noMax,dema.noMax)
	//dema.accumulatedDif= append(dema.accumulatedDif, accumulatedDif...)
	dema , _= decisionHandler(dema,instrument.Candles[:offset])
	orderType:=0
	startRate :=0.0
	//fmt.Println(counter, init,cmd)
	countResult:=make([]int,4,4)
	topLost :=make([]float64,100,100)
	topProfit:=make([]float64,100,100)


	//Test signal 3
	//Delete later

	//out:=0
	//dema,out= signal03Predict(dema,instrument.Candles[:offset])
	//for i:=offset;i<len(instrument.Candles);i++ {
	//	dema,out= signal03Predict(dema,[]candle{instrument.Candles[i]})
	//}
	//writeFile(testsignal3out,"testSignal3OutNew.txt")
	//if out==0{
	//	os.Exit(1)
	//}else {
	//	fmt.Println(out)
	//}

	/////////////////////////////


	for i:=offset;i<len(instrument.Candles);i++ {
		//logContent:=""
		candles:=[]candle{instrument.Candles[i]}
		dema ,cmd= decisionHandler(dema,candles)
		if math.Abs(float64(cmd)) == 1 && orderType != cmd {
			fund, countResult,_ = testTradeResult(fund, testOrder(orderType, fund, startRate, instrument.Candles[i].MidF.C), orderType, countResult)
			orderType = cmd
			startRate = instrument.Candles[i].MidF.C
			fmt.Println("Started Rate: ",startRate," at",instrument.Candles[i].Time, "with order", orderType, "points", dema.increasePt,dema.decreasePt)
		} else if orderType != 0 && cmd == -2 {
			var profitable bool
			fund, countResult,profitable = testTradeResult(fund, testOrder(orderType, fund, startRate, instrument.Candles[i].MidF.C), orderType, countResult)
			orderType = 0
			if !profitable {
				for k:=0;k<len(topLost);k++ {
					if topLost[k]< math.Abs(startRate-instrument.Candles[i].MidF.C) {
						for j:=len(topLost)-1;j>k;j--{
							topLost[j]= topLost[j-1]
						}
						topLost[k]=math.Abs(startRate-instrument.Candles[i].MidF.C)
						break
					}
				}

			}else {
				for k:=0;k<len(topProfit);k++ {
					if topProfit[k]< math.Abs(startRate-instrument.Candles[i].MidF.C) {
						for j:=len(topProfit)-1;j>k;j--{
							topProfit[j]= topProfit[j-1]
						}
						topProfit[k]=math.Abs(startRate-instrument.Candles[i].MidF.C)
						break
					}
				}
			}
			fmt.Println("Ended Rate: ",instrument.Candles[i].MidF.C," at",instrument.Candles[i].Time, ". Points:", dema.increasePt,dema.decreasePt)

		}
		//if log {
		//	totalLog += logContent
		//}
		if enablePermanentFund&& fund> originalFund*1.2 {
			permaFund+=fund-originalFund
			fund=originalFund
		}
		if fund<0.1 {
			fmt.Println("Out of balance")
			break
		}
		if fund<80 && permaFund>80-fund{
			permaFund-=80-fund
			fund=80
		}
		if suddenExit{
			//os.Exit(1)
		}

	}
	//if log {
	//	writeLog("Log",totalLog)
	//}


	layout := "2006-01-02T15:04:05.000000000Z"
	t1, err := time.Parse(layout, instrument.Candles[offset].Time)
	if err!=nil{
		fmt.Println(err)
	}
	if file=="" {
		t1 = t1.Add(-5 * time.Hour)
	}
	t1Str:=fmt.Sprintf("%d-%02d-%02dT%02d:%02d:%02d",	t1.Year(), t1.Month(), t1.Day(),t1.Hour(), t1.Minute(), t1.Second())

	t2, err := time.Parse(layout, instrument.Candles[len(instrument.Candles)-1].Time)
	if err!=nil{
		fmt.Println(err)
	}
	if file=="" {
		t2 = t2.Add(-5 * time.Hour)
	}
	t2Str:=fmt.Sprintf("%d-%02d-%02dT%02d:%02d:%02d",	t2.Year(), t2.Month(), t2.Day(),t2.Hour(), t2.Minute(), t2.Second())


	s:="------------------------------------------------------\n"
	s+="Test from " + t1Str + " to " +t2Str+":\n"
	s+="- Original fund: "+fmt.Sprintf("%.02f$\n",original)
	s+="- Trade Fee: "+fmt.Sprintf("%.06f$/Dollar\n",tradeFee)
	s+="- Outcome fund: "+fmt.Sprintf("%.02f$\n",fund)
	if original>fund{
		s+="- Net Loss: "

	}else{
		s+="- Net Profit: "

	}
	s+=fmt.Sprintf("%0.02f$ (%.02f%%)\n",-original+fund,math.Abs(original-fund)*100/original)
	s+="- Total trade made: " + fmt.Sprintln(countResult[0]+countResult[1]+countResult[2]+countResult[3])
	s+="- Correct trade made             : " +fmt.Sprintf("%v (%.02f%%)\n",countResult[0]+countResult[1],float64(countResult[0]+countResult[1])*100/float64(countResult[0]+countResult[1]+countResult[2]+countResult[3]))
	s+="           Correct Long trade    : " +fmt.Sprintf("%v (%.02f%%)\n",countResult[0],float64(countResult[0])*100/float64(countResult[0]+countResult[1]+countResult[2]+countResult[3]))
	s+="           Correct Short trade   : " +fmt.Sprintf("%v (%.02f%%)\n",countResult[1],float64(countResult[1])*100/float64(countResult[0]+countResult[1]+countResult[2]+countResult[3]))
	s+="- Incorrect trade made           : " +fmt.Sprintf("%v (%.02f%%)\n",countResult[2]+countResult[3],float64(countResult[2]+countResult[3])*100/float64(countResult[0]+countResult[1]+countResult[2]+countResult[3]))
	s+="           Incorrect Long trade  : " +fmt.Sprintf("%v (%.02f%%)\n",countResult[2],float64(countResult[2])*100/float64(countResult[0]+countResult[1]+countResult[2]+countResult[3]))
	s+="           Incorrect Short trade : " +fmt.Sprintf("%v (%.02f%%)\n",countResult[3],float64(countResult[3])*100/float64(countResult[0]+countResult[1]+countResult[2]+countResult[3]))
	s+="------------------------------------------------------\n"
	fmt.Print(s)
	writeFile(s,"")
	fmt.Println("Loss:",lossStop," | Pseudo:",pseudoStop," | Profit:",profitStop," | Double: ", suddenStop," |Patience: ",patienceStop)
	if enablePermanentFund {
		fmt.Println("Perma-Fund:", permaFund)
	}
	return fund-original, topLost, topProfit
}

