package main

import (
	"fmt"
	"math"
	"time"
)

type decisionMatrix struct {
	//*User for all
	currentTrend int
	cachedTrend                        []int //affected by signal 1
	currentDif float64
	previousDif             float64


	//*Use for signal 1
	increasePt,decreasePt              float64
	increaseCon, decreaseCon           int
	originalRate, currentProfitRate, previousProfitRate    float64
	accumulatedDifIncreasing                    []float64
	accumulatedTrendDifIncreasing              []int
	noMax,waitingCount,waitingCountMax int
	//*Use for signal 4
	savedCandles []candle
	startedRSI bool
	previousGain, previousLost float64

	//*Use for signal 3
	nearestHigh,nearestLow                     []float64
	nearestHighCache,nearestLowCache float64

	//*Use for signal 2
	init                             bool
	expectProfit, expectLoss         float64
	accumulatedProfitMultiplier      int
	countPatience, countPatienceLoss int
	previousClose float64
	lossRatio,profitRatio float64

	//
	nearestDif []float64
	totalDeviation float64
}

const nearestDifCount = 600
func scoreCalculator(dema decisionMatrix,c []candle) (decisionMatrix,bool)  {
	for k := 0; k < len(c); k++ {
		if len(dema.nearestDif) == 0 {
			nearestDif:= make([]float64, nearestDifCount,nearestDifCount)
			for i:=0;i<len(nearestDif);i++ {
				nearestDif[i]=-1.0
			}
			dema.nearestDif = append(dema.nearestDif, nearestDif...)
		}
		cd:=math.Abs(c[k].MidF.O -c[k].MidF.C)
		//var filled bool
		//for h:=0;h<len(c);h++ {
			filled:=true
			if dema.nearestDif[len(dema.nearestDif)-1] < 0 {
				filled = false
			}
			if !filled{
				for i:=0;i<len(dema.nearestDif);i++ {
					if dema.nearestDif[i]<0 {
						dema.nearestDif[i]=cd
						break
					}
				}
			}else {
				newNearestDiff:=dema.nearestDif[1:]
				newNearestDiff= append(newNearestDiff, cd)
				dema.nearestDif=nil
				dema.nearestDif=newNearestDiff
			}
		//}
		SUM:=0.0
		for i:=0;i<len(dema.nearestDif);i++ {
			SUM+=dema.nearestDif[i]
		}
		dema.totalDeviation=SUM*100/nearestDifCount

		//<editor-fold desc="Calculate point between candles">
		if c[k].MidF.C > c[k].MidF.O {
			dema.increaseCon++
			dema.decreaseCon = 0
			dema.increasePt += math.Pow(c[k].MidF.C/c[k].MidF.O, float64(fibonacciGen(dema.increaseCon)))
			if dema.decreasePt*dema.increasePt > 0 {
				dema.increasePt -= dema.decreasePt / Golden_ratio
			} else if dema.decreasePt*dema.increasePt < 0 {
				dema.increasePt += dema.decreasePt / Golden_ratio
			}
		} else if c[k].MidF.C < c[k].MidF.O {
			dema.increaseCon = 0
			dema.decreaseCon++
			dema.decreasePt += math.Pow(c[k].MidF.C/c[k].MidF.O, float64(fibonacciGen(dema.decreaseCon)))
			if dema.decreasePt*dema.increasePt > 0 {
				dema.decreasePt -= dema.increasePt / Golden_ratio
			} else if dema.decreasePt*dema.increasePt < 0 {
				dema.decreasePt += dema.increasePt / Golden_ratio
			}
		}
		dema.previousDif = dema.currentDif
		dema.currentDif= math.Abs(dema.increasePt - dema.decreasePt)
	}
	if len(c)<2 {
		return dema,true
	}
	return dema,false
}

func decision(dema decisionMatrix,candles []candle) (decisionMatrix,int) {

	prediction:=0
	result01,result02,result03:=0,0,0
	dema,result01= signal01Predict(dema,candles)
	dema,result02 = predictSignal04Candle(dema,candles)
	dema,result03 =signal03Predict(dema,candles)
	if result01==1 && result02!=11 {
		prediction=1
	}
	if result01==-1 && result02!=-11 {
		prediction=-1
	}

	if math.Abs(float64(prediction))==1 && math.Abs(float64(result03))==1{
		if prediction!=result03 {
			fmt.Println("Switch predicting direction")
		}
		prediction=result03
		prediction=0
	}
	if prediction!=0 && len(candles)<2{
		if !tradingTime(candles[0].Time){
			prediction=0
		}
	}
	return dema,prediction
}

func decisionHandler(dema decisionMatrix, c []candle) (decisionMatrix,int) {
	dema,init:=scoreCalculator(dema,c)
	if !init {
		return dema,0
	}
	var d int
	result:=0
	if dema.currentTrend==0 {
		dema, d = decision(dema, c)
	}
	if d!=0 {
		dema.currentTrend=d
		result=d
	}else {
		dema,result=signal05Predict(dema,c)
		//fmt.Println(result)
	}
	dema.previousClose=c[len(c)-1].MidF.C

	if dema.currentTrend != 0  {
		////currentDif := math.Abs(dema.increasePt - dema.decreasePt)
		//cacheTrendLocal := make([]int, 0, 0)
		//cacheTrendLocal = append(cacheTrendLocal, dema.cachedTrend...)
		//debugTurnCounter++
		//if dema.waitingCountMax == 0 {
		//	dema.waitingCountMax = 3
		//	//dema.currentProfit = 0.0013
		//
		//	if dema.currentTrend ==1 {
		//		dema.currentProfitRate = initial_profit_Increasing
		//	}else {
		//		dema.currentProfitRate = initial_profit_Decreasing
		//	}
		//} else {
		//	profitDif := c[0].MidF.C-c[0].MidF.O
		//	dema.currentProfitRate -= profitDif * float64(dema.currentTrend)
		//	//if (dema.currentTrend >0 && dema.originalRate-c[0].MidF.C> 0.0005)||(dema.currentTrend <0 && dema.originalRate-c[0].MidF.C< -0.0005){
		//	//	//if debugTurnCounter>15 {
		//	//	//	dema = resetDecisionMatrix(dema, []int{0})
		//	//	//	result = -2
		//	//	//	debugTurnCounter = 0
		//	//	//	cacheTrendLocal = []int{}
		//	//	//}
		//	//	//fmt.Println("Prevent loss early")
		//	//}
		//
		//	if (dema.currentTrend > 0 && dema.originalRate+dema.currentProfitRate < c[0].MidF.C) || (dema.currentTrend < 0 && dema.originalRate-dema.currentProfitRate > c[0].MidF.C) {
		//		dema=resetDecisionMatrix(dema,[]int{0})
		//		result = -2
		//		debugTurnCounter = 0
		//		cacheTrendLocal = []int{}
		//	}
		//}
		//
		//if result != -2 {
		//	if dema.waitingCount <= dema.waitingCountMax {
		//		if dema.waitingCount == 0 {
		//			if dema.currentTrend == 1 {
		//				result = 1
		//			} else {
		//				result = -1
		//			}
		//			dema.waitingCount = 1
		//			dema.previousDif = dema.currentDif
		//		} else {
		//			result = 0
		//			dema.previousDif = dema.currentDif
		//		}
		//		dema.waitingCount++
		//		currentTrend := 0
		//		if dema.increasePt > dema.decreasePt {
		//			currentTrend = -1
		//		} else {
		//			currentTrend = 1
		//		}
		//
		//		cacheTrendLocal = append(cacheTrendLocal, currentTrend)
		//
		//	} else {
		//		dema.previousDif = dema.currentDif
		//		count := 0
		//		for i := 0; i < len(dema.cachedTrend); i++ {
		//			if dema.cachedTrend[i] == dema.currentTrend {
		//				count++
		//			}
		//		}
		//		if float64(count)*(1/(2-Golden_ratio)) > float64(dema.waitingCountMax) {
		//			if dema.waitingCountMax < 144 {
		//				dema.waitingCountMax = fibonacciGen(fibonacciSearch(dema.waitingCountMax) + 1)
		//			}
		//			dema.waitingCount = 1
		//			if dema.currentTrend == 1 {
		//				result = 1
		//			} else {
		//				result = -1
		//			}
		//
		//			cacheTrendLocal = []int{}
		//		} else {
		//			//fmt.Println("\t\t\tTurn counter:", debugTurnCounter)
		//			dema=resetDecisionMatrix(dema,[]int{0})
		//			result = -2
		//			debugTurnCounter = 0
		//			cacheTrendLocal = []int{}
		//		}
		//
		//	}
		//
		//}
		//dema.waitingCount++
		//dema.cachedTrend = cacheTrendLocal

	}
	dema.increasePt *= (100.0 - Golden_ratio) / 100.0
	dema.decreasePt *= (100.0 - Golden_ratio) / 100.0
	return dema,result

}

func fibonacciGen(index int) int {
	if index<3 {
		return 1
	}
	count:=2
	value1,value2,value:=1,1,0
	for  {
		value=value1+value2
		value1=value2
		value2=value
		count++
		if count>=index{
			return value2
		}
	}
}

func fibonacciSearch(val int) int {
	count :=2
	value1,value2,value:=1,1,0
	if val==0 {
		return 0
	}else if val==1 {
		rand:=time.Now().Nanosecond()/100
		if rand%2==0 {
			return 1
		}else {
			return 2
		}

	}
	for  {
		value=value1+value2
		value1=value2
		value2=value
		count++
		if value2>=val{
			if value2>val{
				return -1
			}else {
				break
			}
		}
	}
	return count
}
